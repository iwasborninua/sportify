<?php

namespace app\fixtures;

class AddressFixture extends ActiveFixture
{
	public $modelClass = '\\app\\models\\Address';
}