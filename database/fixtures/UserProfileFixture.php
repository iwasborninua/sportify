<?php

namespace app\fixtures;

class UserProfileFixture extends ActiveFixture
{
	public $modelClass = '\\app\\models\\UserProfile';
}