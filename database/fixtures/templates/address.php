<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

$created = time() - rand(0, 3600 * 24 * 60); // not more than 2 months old
return [
    'type'                => mt_rand(0, 99),
    'addresable_type'     => $faker->word,
    'addresable_id'       => mt_rand(1,(int)99999999999),
    'title'               => $faker->title,
    'line1'               => $faker->text($maxNbChars = 200),
    'line2'               => $faker->text($maxNbChars = 200),
    'city'                => $faker->city,
    'state'               => $faker->state,
    'country'             => $faker->stateAbbr,
    'postal_code'         => $faker->postcode,
    'created_by'          => mt_rand(1,(int)99999999999),
    'created_at'          => $created,
    'updated_at'          => time() - rand(0, time() - $created),
];