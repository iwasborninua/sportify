<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

$email = $faker->email;
$created = time() - rand(0, 3600 * 24 * 60); // not more than 2 months old
return [
    'user_id'               => $index + 1,
    'birthdate'             => mt_rand(0, (int)99999999999),
    'about'                 => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'sport_id'              => mt_rand(1,10),
    'sport_title'           => $faker->title,
    'personal_bests'        => $faker->sentence($nbWords = 3, $variableNbWords = true),
    'coach_name'            => $faker->firstName,
    'photo_filename'        => md5(uniqid()) . 'jpg',
    'risk_score'            => mt_rand(0,9),
    'facebook'              => $faker->url,
    'twitter'               => $faker->url,
    'instagram'             => $faker->url,
    'created_at'            => $created,
    'updated_at'            => time() - rand(0, time() - $created)
];