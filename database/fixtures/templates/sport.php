<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

$email = $faker->email;
$created = time() - rand(0, 3600 * 24 * 60); // not more than 2 months old
return [
    'title'             => $faker->title,
    'created_by'        => mt_rand(0,(int)99999999999),
    'created_at'        => $created,
    'updated_at'        => time() - rand(0, time() - $created),
];