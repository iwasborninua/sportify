<?php

namespace app\fixtures;

class SportFixture extends ActiveFixture
{
	public $modelClass = '\\app\\models\\Sport';
}