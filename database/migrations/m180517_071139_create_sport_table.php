<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `sport`.
 */
class m180517_071139_create_sport_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%sport}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->char(127),
            'created_by'        => $this->integer(11),
            'created_at'        => $this->integer(11),
            'updated_at'        => $this->integer(11),
        ], $this->tableOptions());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%sport}}');
    }
}
