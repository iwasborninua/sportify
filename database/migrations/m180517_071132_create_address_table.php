<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `address`.
 */
class m180517_071132_create_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%address}}', [
            'id'                  => $this->primaryKey(),
            'type'                => $this->smallInteger(2),
            'addresable_type'     => $this->char(127),
            'addresable_id'       => $this->integer(11),
            'title'               => $this->char(127),
            'line1'               => $this->char(255),
            'line2'               => $this->char(255),
            'city'                => $this->char(127),
            'state'               => $this->char(127),
            'country'             => $this->char(2),
            'postal_code'         => $this->char(16),
            'created_by'          => $this->integer(11),
            'created_at'          => $this->integer(11),
            'updated_at'          => $this->integer(11),
        ], $this->tableOptions());

        $this->createIndex('ix_addresable', '{{address}}', ['addresable_type', 'addresable_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
