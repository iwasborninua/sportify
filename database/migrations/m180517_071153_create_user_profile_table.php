<?php

use app\modules\base\db\Migration;

/**
 * Handles the creation of table `user_profile`.
 */
class m180517_071153_create_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user_profile}}', [
            'user_id'               => $this->primaryKey(),
            'birthdate'             => $this->integer(11),
            'about'                 => $this->text(),
            'sport_id'              => $this->integer(11),
            'sport_title'           => $this->string(),
            'personal_bests'        => $this->text(),
            'coach_name'            => $this->string(),
            'photo_filename'        => $this->string(),
            'risk_score'            => $this->string(),
            'facebook'              => $this->string(),
            'twitter'               => $this->string(),
            'instagram'             => $this->string(),
            'created_at'            => $this->integer(11),
            'updated_at'            => $this->integer(11),
        ], $this->tableOptions());

        $this->addForeignKey(
            'fk-user_profile-user',
            'user_profile',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-user_profile-sport',
            'user_profile',
            'sport_id',
            'sport',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%user_profile}}');
        $this->dropForeignKey('fk-user_profile-user', 'user');
        $this->dropForeignKey('fk-user_profile-sport', 'sport');
    }
}
