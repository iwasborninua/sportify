<?php

use app\modules\base\db\Migration;

/**
 * Class m180530_192418_add_user_email_confirm_token
 */
class m180530_192418_add_user_email_confirm_token extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function safeUp()
	{
        $this->addColumn('{{%user}}', 'email_confirm_token', $this->string()->unique()->after('email'));
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown()
	{
        $this->dropColumn('{{%user}}', 'email_confirm_token');
	}
}
