<?php

namespace app\assets;

    class AssetBundle extends \yii\web\AssetBundle
{
	public $sourcePath = '@app/assets';

	public $css = [
        'css/uikit.css',
        'css/style.css',
	];
	public $js = [
        'js/uikit.js',
        'js/uikit-icons.js',
	];


	public $depends = [
		'yii\web\YiiAsset',
//		'yii\bootstrap\BootstrapAsset',
	];
}
