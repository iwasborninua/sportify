<?php

/* @var $athlete app\models\UserProfile */

$this->title = 'Athlete';
?>

<div class="uk-section">
    <div class="uk-container uk-flex uk-flex-between uk-flex-middle">
        <div>
            <h2>Athlete</h2>
        </div>
        <div>
            <ul class="uk-breadcrumb">
                <li><a href="#">Home</a></li>
                <li><span>Athlete</span></li>
            </ul>
        </div>
    </div>
</div>

<section class="uk-margin-bottom" uk-grid>
    <div class="uk-width-1-3">
        <h4>Athlete information</h4>
        <p>birthdate:</p>
        <p><?= $athlete->birthdate?></p>
        <p>about:</p>
        <p><?=$athlete->about?></p>
        <p>personal_bests:</p>
        <p><?=$athlete->personal_bests?></p>
        <p>coach name:</p>
        <p><?=$athlete->coach_name?></p>
        <p>risk_score:</p>
        <p><?=$athlete->risk_score?></p>
        <p>facebook:</p>
        <p><?=$athlete->facebook?></p>
        <p>twitter:</p>
        <p><?=$athlete->twitter?></p>
        <p>instagram:</p>
        <p><?=$athlete->instagram?></p>
    </div>
    <div class="uk-width-2-3">
        <div class="uk-background-cover uk-height-medium" style="background-image: url('/img/<?= $athlete->photo_filename ?>');">
    </div>
</section>


