<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\forms\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title                   = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>


	<h1 class="uk-margin-large-top"><?= Html::encode($this->title) ?></h1>

	<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="uk-alert-success" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>Thank you for contacting us. We will respond to you as soon as possible.</p>
        </div>

		<p>
			Note that if you turn on the Yii debugger, you should be able
			to view the mail message on the mail panel of the debugger.
			<?php if (Yii::$app->mailer->useFileTransport) : ?>
				Because the application is in development mode, the email is not sent but saved as
				a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
																									Please configure the
				<code>useFileTransport</code> property of the <code>mail</code>
				application component to be false to enable email sending.
			<?php endif; ?>
		</p>

	<?php else: ?>

		<p>
			If you have business inquiries or other questions, please fill out the following form to contact us.
			Thank you.
		</p>


            <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'layout' => 'horizontal',
                ]); ?>

            <?= $form->field($model, 'name', [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2@m'
                ]])->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email', [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2@m'
                ]]) ?>

            <?= $form->field($model, 'subject', [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2@m'
                ]]) ?>

            <?= $form->field($model, 'body', [
                'inputOptions' => [
                    'class' => 'uk-textarea uk-width-1-2@m'
                ]])->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'verifyCode')->widget(Captcha::class, [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                'options' => [
                    'class' => 'uk-input uk-width-1-2@m'
                ]
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'class => uk-button uk-margin-large-bottom', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

	<?php endif; ?>


