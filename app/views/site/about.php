<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title                   = 'About';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="uk-section">
    <div class="uk-container uk-flex uk-flex-between uk-flex-middle">
        <div>
            <h2>About Sportify</h2>
        </div>
        <div>
            <ul class="uk-breadcrumb">
                <li><a href="#">Home</a></li>
                <li><span>About</span></li>
            </ul>
        </div>
    </div>
</div>

<section class="uk-grid-small uk-child-width-expand@s uk-margin-large-bottom" uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-text-center">
            <span uk-icon="icon: users; ratio: 2"></span>
            <h2>Best team</h2>
            <p>
                Dolorem adipiscing definiebas ut nec. Dolore consectetuer eu vim, elit molestie ei has, petentium imperdiet in pri.
                Mel virtute efficiantur ne, zril omnes sed no, sit eu duis semper.
            </p>
        </div>
    </div>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-text-center">
            <span uk-icon="icon: star; ratio: 2"></span>
            <h2>High dedication</h2>
            <p>
                Dolorem adipiscing definiebas ut nec. Dolore consectetuer eu vim, elit molestie ei has, petentium imperdiet in pri.
                Mel virtute efficiantur ne, zril omnes sed no, sit eu duis semper.
            </p>
        </div>
    </div>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-text-center">
            <span uk-icon="icon: heart; ratio: 2"></span>
            <h2>Work with heart</h2>
            <p>
                Dolorem adipiscing definiebas ut nec. Dolore consectetuer eu vim, elit molestie ei has, petentium imperdiet in pri.
                Mel virtute efficiantur ne, zril omnes sed no, sit eu duis semper.
            </p>
        </div>
    </div>
</section>

<hr class="uk-margin-large-bottom">

<section class="uk-margin-large-bottom">
    <h2 class="uk-margin-large-bottom">Our services</h2>
    <ul uk-accordion>
        <li>
            <a class="uk-accordion-title" href="#">Database management</a>
            <div class="uk-accordion-content">
                <p>
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                    probably haven't heard of them accusamus labore sustainable VHS.
                </p>
            </div>
        </li>
        <li>
            <a class="uk-accordion-title" href="#">UI development</a>
            <div class="uk-accordion-content">
                <p>
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                    probably haven't heard of them accusamus labore sustainable VHS.
                </p>
            </div>
        </li>
        <li>
            <a class="uk-accordion-title" href="#">Social media</a>
            <div class="uk-accordion-content">
                <p>
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                    3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                    Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                    Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                    probably haven't heard of them accusamus labore sustainable VHS.
                </p>
            </div>
        </li>
    </ul>
</section>

