<?php

/* @var $athlete app\models\UserProfile */
/* @var $sport app\models\Sport */

$this->title = 'Athletes';
?>

<div class="uk-section">
    <div class="uk-container uk-flex uk-flex-between uk-flex-middle">
        <div>
            <h2>Athletes</h2>
        </div>
        <div>
            <ul class="uk-breadcrumb">
                <li><a href="#">Home</a></li>
                <li><span>Athletes</span></li>
            </ul>
        </div>
    </div>
</div>

<div uk-filter="target: .js-filter" class="uk-margin-large-bottom">

    <ul class="uk-subnav uk-subnav-pill">
        <?php foreach ($sports as $sport):?>
            <li uk-filter-control="<?=$sport->title?>"><a href="#"><?=$sport->title?></a></li>
        <?php endforeach;?>
    </ul>

    <ul class="js-filter uk-child-width-1-3@m uk-text-center" uk-grid>
        <?php foreach ($athletes as $athlete):?>
            <li class="<?=$athlete->sport_title?>">
                <a href="<?=\yii\helpers\Url::to(['site/athlete', 'id' => $athlete->user_id])?>" style="display: block; text-decoration: none;">
                    <div class="uk-card uk-card-default uk-card-body uk-padding-remove">
                        <div class="uk-background-cover uk-height-medium" style="background-image: url('/img/<?= $athlete->photo_filename ?>');">
                        </div>
                    </div>
                    <div class="uk-card uk-card-default uk-card-footer">
                        <h2><?=$athlete->sport_title?></h2>
                        <p><?=$athlete->about?></p>
                    </div>
                </a>
            </li>
        <?php endforeach;?>
    </ul>

</div>

