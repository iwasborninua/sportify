<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
	<h1 class="uk-margin-large-top uk-margin-large-bottom uk-text-center"><?= Html::encode($this->title) ?></h1>

	<div class="uk-alert uk-alert-danger">
		<?= nl2br(Html::encode($message)) ?>
	</div>

	<p class="uk-alert uk-alert-large uk-margin-large-bottom">
		The above error occurred while the Web server was processing your request.<br>
        Please contact us if you think this is a server error. Thank you.
	</p>
