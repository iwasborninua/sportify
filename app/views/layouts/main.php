<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\FlashAlert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AssetBundle;

AssetBundle::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<?=$this->render('/partials/header')?>

<!--    <div id="offcanvas" class="uk-offcanvas">-->
<!--        <div class="uk-offcanvas-bar">-->
<!--            <ul class="uk-nav uk-nav-offcanvas">-->
<!--                <li class="uk-active">-->
<!--                    --><?//=Html::a('Home')?>
<!--                </li>-->
<!--                <li>-->
<!--                    --><?//=Html::a('About Us')?>
<!--                </li>-->
<!--                <li>-->
<!--                    --><?//=Html::a('Athletes')?>
<!--                </li>-->
<!--                <li>-->
<!--                    --><?//=Html::a('Contact')?>
<!--                </li>-->
<!--                <li>-->
<!--                    --><?php //if (Yii::$app->user->isGuest)
//                        echo Html::a('Login');
//                    else
//                        echo Html::a('Logout')
//                    ?>
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->

<div class="uk-container uk-container-center">
    <?=$content;?>
</div>


<?=$this->render('/partials/footer')?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
