<?php

use app\models\User;
use app\models\UserProfile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\Sport;
use yii\helpers\ArrayHelper;

/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\forms\UserForm
 * @var $profile \app\models\UserProfile$
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="user-form box uk-margin-top">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'inputOptions' => [
                'class' => 'uk-input uk-width-1-1',
            ],
        ]
    ]); ?>

        <?= $form->field($model, 'birthdate')->textInput() ?>

        <?= $form->field($model, 'about')->textarea() ?>

        <?= $form->field($model, 'sport_id')->dropDownList(ArrayHelper::map(Sport::find()->all(),'id', 'title')) ?>

        <?= $form->field($model, 'personal_bests')->textarea() ?>

        <?= $form->field($model, 'coach_name')->textInput() ?>

        <?= $form->field($model, 'photo_filename', [
	        'inputOptions' => [
                'class' => ''
            ]
    ])->fileInput() ?>

        <?= $form->field($model, 'facebook')->textInput() ?>

        <?= $form->field($model, 'twitter')->textInput() ?>

        <?= $form->field($model, 'instagram')->textInput() ?>

        <?= Html::submitButton('Save', [
                'class' => 'uk-button uk-margin-large-bottom',
                'name'  => 'save',
                'value' => 1])?>

    <?php ActiveForm::end(); ?>
</div>
