<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;

?>

<footer>
    <div class="uk-container uk-container-center">
        <div class="uk-child-width-1-2@s" uk-grid>
            <div class="uk-margin-large-top uk-margin-large-bottom">
                <h2>Plato</h2>
                <p>Plato business company, Inc.<br>
                    4455 Great building Ave, Suite A10<br>
                    San Francisco, CA 94107<br>
                    P: (123) 456-7890 </p>
            </div>
            <div class="uk-margin-large-top uk-margin-large-bottom">
                <div class="uk-child-width-1-2@s" uk-grid>
                    <div>
                        <h2>Browse pages</h2>
                        <p>Lorem ipsum dolor sit amet</p>
                        <p>Tamquam ponderum at eum, nibh dicta offendit mei</p>
                        <p>Vix no vidisse dolores intellegam</p>
                        <p>Est virtute feugiat accommodare eu</p>
                    </div>
                    <div>
                        <h2>Keep updated</h2>
                        <p>Enter your email to subcribe newsletter </p>
                        <form>
                            <div class="uk-margin">
                                <div class="uk-inline">
                                    <a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: pencil"></a>
                                    <input class="uk-input" type="text">
                                </div>
                            </div>
                        </form>
                        <div class="social_icons">
                            <a class="uk-margin-small-right" href="#" uk-icon="icon: facebook"></a>
                            <a class="uk-margin-small-right" href="#" uk-icon="icon: twitter"></a>
                            <a class="uk-margin-small-right" href="#" uk-icon="icon: linkedin"></a>
                            <a class="uk-margin-small-right" href="#" uk-icon="icon: pinterest"></a>
                            <a class="uk-margin-small-right" href="#" uk-icon="icon: google-plus"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>