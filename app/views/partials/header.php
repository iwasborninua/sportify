<?php

use yii\helpers\Html;

?>

<header>
    <div class="uk-container">
        <nav uk-navbar>

            <div class="uk-navbar-left">

                <ul class="uk-navbar-nav">
                    <li><?=Html::a('Home',['site/index'], ['class' => 'uk-padding-remove-left'])?></li>
                    <li><?=Html::a('Athletes', ['site/athletes'])?></li>
                    <li><?=Html::a('About us', ['site/about'])?></li>
                    <li><?=Html::a('Contact', ['site/contact'])?></li>
                </ul>

            </div>

            <div class="uk-navbar-right">

                <ul class="uk-navbar-nav">
                    <?php if(Yii::$app->user->isGuest):?>
                        <li><?=Html::a('Login', ['auth/login'])?></li>
                        <li><?=Html::a('Register', ['auth/register'])?></li>
                    <?php else:?>
                        <li><?=Html::a('Profile', ['profile/index'])?></li>
                        <li><?=Html::a('Logout', ['auth/logout'])?></li>
                    <?php endif;?>
                </ul>

            </div>

        </nav>
    </div>
</header>


