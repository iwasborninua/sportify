<?php

use yii\helpers\Html;
use app\models\Survey;

?>

        <?= $form->field($model, 'question3')
            ->radioList(Survey::getRadioValues('question3'))
            ->label('How many years has it been since you first started practicing your most important lifestyle sport?')?>


        <?= $form->field($model, 'question4')
            ->radioList(Survey::getRadioValues('question4'))
            ->label('Do you practice your most important lifestyle sport individually or in groups?')?>


        <?= $form->field($model, 'question5')
            ->radioList(Survey::getRadioValues('question5'))
            ->label('With who do you practice your favorite lifestyle sport? Multiple answers possible.')?>


        <?= $form->field($model, 'question6')
            ->radioList(Survey::getRadioValues('question6'))
            ->label('What is the reason you practice your favorite lifestyle sport? Select your answer, or answer other if applicable.')?>


        <?= $form->field($model, 'question7')
            ->radioList(Survey::getRadioValues('question7'))
            ->label('How many trainings do you have per week?')?>

    <input type="hidden" name="step" value="2">

    <?= Html::submitButton('Previos', [
        'class' => 'uk-button uk-button-default uk-margin-large-bottom',
        'name' => "go-back",
        'value' => 1
    ])?>

    <?= Html::submitButton('Next', [
        'class' => 'uk-button uk-button-default uk-margin-large-bottom'
    ])?>

