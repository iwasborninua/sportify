<?php

use yii\helpers\Html;
use app\models\Survey;

?>

            <?= $form->field($model, 'question1')
                ->radioList(Survey::getRadioValues('question1'))
                ->label('What is your current occupation?')?>

            <?= $form->field($model, 'question2')
                ->radioList(Survey::getRadioValues('question2'))
                ->label('Which educational level did you finish last?')?>


    <input type="hidden" name="step" value="1">

    <?= Html::submitButton('Next', [
            'class' => 'uk-button uk-button-default uk-margin-large-bottom'
    ])?>
