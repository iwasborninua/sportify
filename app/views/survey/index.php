<?php

    use yii\bootstrap\ActiveForm;

?>

    <?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "<section class = 'uk-margin-top uk-margin-bottom'>\n{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}\n</section>",
        'horizontalCssClasses' => [
            'label' => '',
            'offset' => '',
            'wrapper' => '',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

        <?= $this->render($view, [
            'model' => $model,
            'form' => $form,
    ]);?>

<?php ActiveForm::end()?>