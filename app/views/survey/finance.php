<?php

    use yii\helpers\Html;
    use app\models\Survey;

?>

        <?= $form->field($model, 'question8')
            ->radioList(Survey::getRadioValues('question8'))
            ->label('How much money do you roughly spend on average on your most important lifestyle sport per month? 
            (This includes the sport equipment needed, possible fees for use of sport parks/areas, possible training fees, etc.)')?>

        <?= $form->field($model, 'question9')
            ->radioList(Survey::getRadioValues('question9'))
            ->label('If you had no responsibilities considering education and/or work, how much time would you ideally like 
            to spend on lifestyle sports?')?>

    <input type="hidden" name="step" value="3">

    <?= Html::submitButton('Previos', [
        'class' => 'uk-button uk-button-default uk-margin-large-bottom',
        'name' => "go-back",
        'value' => 1
    ])?>

    <?= Html::submitButton('Send', [
        'class' => 'uk-button uk-button-default uk-margin-large-bottom'
    ])?>