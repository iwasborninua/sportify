<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

    $addForm = ActiveForm::begin([
        'options' => [
            'class' => 'uk-form-stacked uk-margin-top uk-margin-bottom']
    ]);

        echo $addForm->field($model, "title", [
            'inputOptions' => [
                'class' => 'uk-input uk-width-1-2'
            ],
            'labelOptions' => [
                'class' => 'uk-form-label'
            ]])->label();

        echo $addForm->field($model, "city", [
            'inputOptions' => [
                'class' => 'uk-input uk-width-1-2'
            ],
            'labelOptions' => [
                'class' => 'uk-form-label'
            ]])->label();

        echo $addForm->field($model, "postal_code", [
            'inputOptions' => [
                'class' => 'uk-input uk-width-1-2'
            ],
            'labelOptions' => [
                'class' => 'uk-form-label'
            ]])->label();

        echo Html::submitButton('Add', [
            'class' => 'uk-button uk-margin-top uk-margin-right',
            'name' => 'add'
        ]);


    ActiveForm::end();

    echo "<hr>";

    foreach ($addresses as $address => $field) {

        $form = ActiveForm::begin([
            'options' => [
                'class' => 'uk-form-stacked uk-margin-top uk-margin-bottom']
        ]);

            echo $form->field($field, "[$address]title", [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2'
                ],
                'labelOptions' => [
                    'class' => 'uk-form-label'
                ]])->label();

            echo $form->field($field, "[$address]city", [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2'
                ],
                'labelOptions' => [
                    'class' => 'uk-form-label'
                ]])->label();

            echo $form->field($field, "[$address]postal_code", [
                'inputOptions' => [
                    'class' => 'uk-input uk-width-1-2'
                ],
                'labelOptions' => [
                    'class' => 'uk-form-label'
                ]])->label();

            echo Html::submitButton('Remove', [
                'class' => 'uk-button uk-margin-top',
                'name' => 'remove',
                'value' => $address,
            ]);

        ActiveForm::end();
    }


