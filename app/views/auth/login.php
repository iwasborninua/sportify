<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\forms\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
	<h1 class="uk-margin-large-top"><?= Html::encode($this->title) ?></h1>

	<p>Please fill out the following fields to login:</p>
	
	<?php $form = ActiveForm::begin([
		'id'          => 'login-form',
		'layout'      => 'horizontal',
        'options' => ['class' => '']
	]); ?>
	
	<?= $form->field($model, 'username', [
	        'inputOptions' => [
                'class' => 'uk-input uk-width-1-2'
            ]
    ])->textInput(['autofocus' => true]) ?>
	
	<?= $form->field($model, 'password', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->passwordInput() ?>
	
	<?= $form->field($model, 'rememberMe')->checkbox([
		'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
	]) ?>

	<div class="form-group">
		<div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'uk-button uk-margin-bottom', 'name' => 'login-button']) ?>
		</div>
	</div>
	
	<?php ActiveForm::end(); ?>
	

		<div>
			<p>Don't have an account? <?= Html::a('Register!', ['auth/register']) ?></p>
		</div>


		<div class="uk-margin-large-bottom">
			<p>Forgot your password? <?= Html::a('Restore!', ['auth/password-request']) ?></p>
		</div>
