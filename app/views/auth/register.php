<?php

/**
 * @var \yii\web\View $this
 * @var \app\forms\RegisterForm $model
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
$options = ['class' => 'hui']
?>

	<h1 class="uk-margin-large-top"><?= Html::encode($this->title) ?></h1>

	<p>Please fill out the following fields to login:</p>
	
	<?php $form = ActiveForm::begin([
		'id'          => 'register-form',
		'layout'      => 'horizontal',
        'options' => ['class' => 'uk-form']
	]); ?>
	
	<?= $form->field($model, 'email', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->textInput([
		'autofocus' => true,
		'type'      => 'email',
	]) ?>

	<?= $form->field($model, 'firstName', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->textInput() ?>

	<?= $form->field($model, 'lastName', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->textInput() ?>

	<?= $form->field($model, 'password', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->passwordInput() ?>

	<?= $form->field($model, 'passwordRepeat', [
        'inputOptions' => [
            'class' => 'uk-input uk-width-1-2'
        ]])->passwordInput() ?>


	<div class="form-group">
		<div class="uk-margin-bottom">
			<?= Html::submitButton('Register', ['class' => 'uk-button']) ?>
		</div>
	</div>
	
	<?php ActiveForm::end(); ?>

	<div class="row">
		<div class="uk-margin-large-bottom">
			<p>Already have an account? <?= Html::a('Login!', ['auth/login']) ?></p>
		</div>
	</div>