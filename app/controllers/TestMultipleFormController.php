<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\web\Response;
use app\models\Address;

class TestMultipleFormController extends Controller
{
	public function actionIndex()
	{
        $addresses = Address::find()->indexBy('id')->all();
        $model = new Address();
        $add = Yii::$app->request->post('add');
        $remove = Yii::$app->request->post('remove');

        if (isset($add))
        {
            if ($model->load(Yii::$app->request->post('add')))
            {
                $model->save(false);
                return $this->redirect('index');
            }
        }

        if (isset($remove)) {
            $id = $remove;
            $model = Address::findOne($id);
            $model->delete();

            return $this->redirect('index');
        }

        if (Model::loadMultiple($addresses, Yii::$app->request->post())) {
            foreach ($addresses as $address) {
                $address->save(false);
            }
            return $this->render('index', ['addresses' => $addresses]);
        }



        return $this->render('index', ['addresses' => $addresses,
            'model' => $model
        ]);
	}	
}