<?php

namespace app\controllers;

use app\models\Survey;
use Yii;


class SurveyController extends Controller
{
    public function actionIndex()
    {
        $post = Yii::$app->request->post();
        $request = Yii::$app->request;
        $step = $request->post('step');
        $back = $request->post('go-back');
        $survey = $request->post('Survey');
        $session = Yii::$app->session;
        
        $model = new Survey();

        if (!isset($step)) {
        $step = 0;
    }

        if (!isset($back)) {
            $model->setAttributes($survey, false);
            
            $rates = $session->get('rates') ?: [];
            $rates[$step] = $model->calculateRate();
            $session->set('rates', $rates);
        } else {
            $step = $step > 1 ? ($step - 2) : 0;
        }

        switch ((int)$step) {
            case 0:
                return $this->render('index', ['model' => $model, 'view' => 'about-us']);
                break;
            case 1:
                return $this->render('index', ['model' => $model, 'view' => 'your-sport']);
                break;
            case 2:
                return $this->render('index', ['model' => $model, 'view' => 'finance']);
                break;
            default:
                $val = array_sum($session->get('rates'));
                $model->scoreCalcuation($val);
                return $this->redirect('/site/index');
                break;
        }
    }
}