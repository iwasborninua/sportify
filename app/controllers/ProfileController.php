<?php

namespace app\controllers;


use app\models\Sport;
use Yii;
use app\models\UserProfile;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{

	    $model = $this->findOrCreateModel(Yii::$app->user->id);

	    if (Yii::$app->request->post('save') && $model->load(Yii::$app->request->post()))
        {
            if (null !== $model->sport_title)
            {
                $sport = Sport::findOne($model->sport_id);
                $model->sport_title = $sport->title;
            }


            if ($image = UploadedFile::getInstance($model, 'photo_filename'))
            {
                $imageName = md5(time()) . '.' . $image->getExtension();
                $savePath = Yii::getAlias('@webroot/img/') . $imageName;
                $image->saveAs($savePath);
                $model->photo_filename = $imageName;
            }

            $model->save(false);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
	}

	public function findOrCreateModel($id)
    {
        if (null !== $model = UserProfile::find()->where(['user_id' => $id])->one()) {
            return $model;
        } else {
            $model = new UserProfile();
            $model->user_id = $id;
            return $model;
        }

    }

}
