<?php

namespace app\controllers;

use app\models\helpers\SignupService;
use app\models\Sport;
use app\models\User;
use app\models\UserProfile;
use Yii;
use yii\web\Response;
use app\forms\ContactForm;

class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex()
	{

	    $users = User::find()->asArray()->with('sport')->all();


		return $this->render('index');
	}

	/**
	 * Displays contact page.
	 *
	 * @return Response|string
	 */
	public function actionContact()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact()) {
			Yii::$app->session->setFlash('contactFormSubmitted');

			return $this->refresh();
		}

		return $this->render('contact', [
			'model' => $model,
		]);
	}

	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout()
	{
		return $this->render('about');
	}

    public function actionAthletes()
    {
        $athletes = UserProfile::find()->all();
        $sports = Sport::find()->all();

        return $this->render('athletes', [
            'athletes' => $athletes,
            'sports' => $sports,
        ]);
    }

    public function actionAthlete($id)
    {
        $model = UserProfile::find()->where(['user_id' => $id])->one();

        return $this->render('athlete', [
            'athlete' => $model
        ]);
    }

    public function actionSignupConfirm($token)
    {
        $signupService = new SignupService();

        try{
            $signupService->confirmation($token);
            Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration.');
        } catch (\Exception $e){
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->goHome();
    }
}
