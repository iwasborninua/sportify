<?php

use app\models\Address;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Users';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
    echo Tabs::widget([
    'items' => [
        [
            'label' => 'Personal Information',
            'content' => $this->render('tabs/personal', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]),
            'active' => true,
        ],

        [
            'label' => 'Training Places',
            'content' => $this->render('tabs/training', [
                'address' => $address
            ]),
        ],
    ],
]);
?>
