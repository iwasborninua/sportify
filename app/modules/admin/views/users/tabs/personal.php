<?php

use app\models\User;
use app\modules\admin\widgets\LinkedColumn;
use yii\helpers\Html;
use app\modules\admin\assets\ThemeHelper;
use app\modules\admin\widgets\BoxGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="user-index">
    <?php $this->beginBlock(ThemeHelper::BLOCK_HEADER_BUTTONS); ?>
        <?= Html::a('Add New', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
    <?php $this->endBlock(); ?>

    <?= BoxGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'header' => '#', 
                'content' => function ($model, $key, $index, $column) { 
                    $pagination = $column->grid->dataProvider->getPagination();
                    $index = $pagination !== false
                        ? $pagination->getOffset() + $index + 1
                        : $index + 1;
                        
                    return Html::a($index, ['users/update', 'id' => $model->id]);
                }
            ],
            [
                'class' => LinkedColumn::class,
                'header' => 'Name',
                'attribute' => 'full_name',
                'value' => 'fullName',
            ],
            'username',
            'email',
            array(
                'attribute' => 'status',
                'value' => 'statusAlias',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    User::getStatusesList(),
                    ['prompt' => 'All', 'class' => 'form-control']
                ),
            ),
            'created_at:date:Registered',
            // 'updated_at',
            [
                'attribute' => 'sport.title',
                'label' => 'Sport',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'sport_id',
                    \app\models\Sport::find()->select(['title'])->indexBy('id')->column(),
                    ['prompt' => 'All', 'class' => 'form-control']
                ),
            ],
        ],
    ]); ?>

</div>