<?php

use app\models\User;
use app\models\UserProfile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\models\Sport;
use yii\helpers\ArrayHelper;

/**
 * @var $this yii\web\View
 * @var $model \app\modules\admin\forms\UserForm
 * @var $profile \app\models\UserProfile$
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="user-form box">
	
	<?php $form = ActiveForm::begin([
		'layout' => 'horizontal',
	]); ?>

	<div class="box-body">
		<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
		
		<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
		
		<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
		
		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		
		<?= $form->field($model, 'password')->passwordInput() ?>
		
		<?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

		<?= $form->field($profile, 'birthdate')->textInput() ?>
		<?= $form->field($profile, 'about')->textarea() ?>
		<?= $form->field($profile, 'sport_id')->dropDownList(ArrayHelper::map(Sport::find()->asArray()->all(), 'id', 'title')) ?>
		<?= $form->field($profile, 'sport_title')->textInput() ?>
		<?= $form->field($profile, 'personal_bests')->textarea() ?>
		<?= $form->field($profile, 'coach_name')->textInput() ?>
		<?= $form->field($profile, 'photo_filename')->textInput() ?>
		<?= $form->field($profile, 'facebook')->textInput() ?>
		<?= $form->field($profile, 'twitter')->textInput() ?>
		<?= $form->field($profile, 'instagram')->textInput() ?>

		<?= $form->field($model, 'status')->dropDownList(User::getStatusesList()) ?>
		
		<?= $form->field($model, 'roles')->dropDownList(User::getRolesList(), [
			'multiple' => 'multiple',
		]) ?>
	</div>
	<div class="box-footer text-right">
		<?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-success']) ?>

        <?php
            if (Yii::$app->controller->action->id == 'update')
            {
                echo Html::submitButton('Del', ['class' => 'btn btn-danger', 'name' => 'del', 'value' => 1]);
            }
        ?>
	</div>
	
	<?php ActiveForm::end(); ?>
</div>
