<?php

namespace app\modules\admin\controllers;

use app\models\Address;
use app\models\User;
use app\models\UserProfile;
use app\modules\admin\forms\UserForm;
use app\modules\admin\models\UserSearch;
use app\traits\controllers\FindModelOrFail;
use Yii;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
	use FindModelOrFail;
	
	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		$this->modelClass = UserForm::class;
	}
	
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}
	
	/**
	 * Lists all User models.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $address = new Address();

        if (Yii::$app->request->post('add'))
        {
            if ($address->load(Yii::$app->request->post()))
            {
                $address->save(false);
                return $this->redirect('users');
            }
        }
		
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
            'address'      => $address,
		]);
	}


	
	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new UserForm();
		$profile = new UserProfile();
		
		$model->on(User::EVENT_BEFORE_INSERT, function ($event) use ($model) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
        });
		
		if (($model->load(Yii::$app->request->post()) && $model->save()) && ($profile->load(Yii::$app->request->post()) && $profile->save())) {
			return $this->redirect(['update', 'id' => $model->id]);
		}
		
		return $this->render('create', [
			'model' => $model,
            'profile' => $profile
		]);
	}
	
	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id)
	{

        if (Yii::$app->request->post('del')) {
            $this->actionDelete($id);
        }

		$model = $this->findModel($id);
		$profile = $model->userProfile ?? new UserProfile();
		
		if (($model->load(Yii::$app->request->post()) && $model->save()) && ($profile->load(Yii::$app->request->post()) && $profile->save())) {
			return $this->redirect(['update', 'id' => $model->id]);
		}

		return $this->render('update', [
			'model' => $model,
            'profile' => $profile,
		]);
	}
	
	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}
	
}
