<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $user_id
 * @property int $birthdate
 * @property string $about
 * @property int $sport_id
 * @property string $sport_title
 * @property string $personal_bests
 * @property string $coach_name
 * @property string $photo_filename
 * @property string $risk_score
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Sport $sport
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birthdate', 'sport_id', 'created_at', 'updated_at'], 'integer'],
            [['about', 'personal_bests'], 'string'],
            [['sport_title', 'coach_name', 'photo_filename', 'risk_score', 'facebook', 'twitter', 'instagram'], 'string', 'max' => 255],
            [['sport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sport::className(), 'targetAttribute' => ['sport_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'birthdate' => 'Birthdate',
            'about' => 'About',
            'sport_id' => 'Sport ID',
            'sport_title' => 'Sport Title',
            'personal_bests' => 'Personal Bests',
            'coach_name' => 'Coach Name',
            'photo_filename' => 'Photo Filename',
            'risk_score' => 'Risk Score',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(Sport::class, ['id' => 'sport_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

}
