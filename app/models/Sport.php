<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sport".
 *
 * @property int $id
 * @property string $title
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property UserProfile[] $userProfiles
 */
class Sport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sport}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 127],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::class, ['sport_id' => 'id']);
    }
}
