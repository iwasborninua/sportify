<?php

namespace app\models;


use yii\base\Model;
use Yii;

class Survey extends Model
{
    public $question1;
    public $question2;
    public $question3;
    public $question4;
    public $question5;
    public $question6;
    public $question7;
    public $question8;
    public $question9;

    static function getRadioValues($question)
    {
        return array_keys(self::$radioMap[$question]);
    }

    public function calculateRate()
    {
        $totalRate = 0;
        
        foreach (self::$radioMap as $question => $options) {
            if ($this->{$question} !== null) {
                $totalRate += array_values($options)[$this->{$question}];
            }
        }
        return $totalRate;
    }

    public function scoreCalcuation($totalRate)
    {
        $risk_score = '';



        if ($totalRate >= 27)
            $risk_score = 'A (lorer risk)';
        elseif ($totalRate >= 21 && $totalRate <= 26)
            $risk_score = 'B';
        elseif ($totalRate >= 12 && $totalRate <= 20)
            $risk_score = 'C';
        else
            $risk_score = 'D';

        if (($model = UserProfile::findOne(Yii::$app->user->id)) === null)
        {
            $userProfile = new UserProfile();
            $userProfile->user_id = Yii::$app->user->id;
            $userProfile->risk_score = $risk_score;
            return $userProfile->save();
        } else {
            $userProfile = UserProfile::findOne('5');
            $userProfile->user_id = Yii::$app->user->id;
            $userProfile->risk_score = $risk_score;
            return $userProfile->save();
        }
    }

    public static $radioMap = [
        'question1' => [
            'Student' => 3,
            'Employed' => 2,
            'Unemployed' => 0,
            'Voluntary work' => 1,
            'Freelancer/entrepreneur (zzp\'er)' => 2,
        ],

        'question2' => [
            'School' => 1,
            'High School' => 3,
            'Colledge' => 2,
            'University' => 2,
        ],

        'question3' => [
            '<1 Year' => 0,
            '1-3 Years' => 1,
            '4-5 Years' => 2,
            '6-10 Years' => 4,
            '>10 Years' => 4,
        ],

        'question4' => [
            'Individually' => 2,
            'In groups' => 3,
            'Both' => 1,
        ],

        'question5' => [
            'Friends' => 2,
            'Family' => 1,
            'Association/fraternity/club' => 3,
        ],

        'question6' => [
            'Personal motivation' => 3,
            'Group motivation' => 2,
            'Interest in others (social)' => 1,
            'Forced' => 0,
            'Other' => 1,
        ],

        'question7' => [
            '1 time' => 0,
            '2 times' => 2,
            '3 times' => 4,
            '4 times' => 6,
        ],

        'question8' => [
            '< $50' => 0,
            '$50-$100' => 1,
            '$100-250' => 3,
            '$250-$500' => 3,
            '$500+' => 2,
        ],

        'question9' => [
            '0-25% of your time' => 0,
            '26-50% of your time' => 1,
            '51-75% of your time' => 3,
            '76%- all of your time' => 2,
        ]
    ];
}