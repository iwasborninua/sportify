<?php

namespace app\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property int $type
 * @property string $addresable_type
 * @property int $addresable_id
 * @property string $title
 * @property string $line1
 * @property string $line2
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $postal_code
 * @property int $created_by
 * @property int $created_at
 * @property int $updated_at
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%address}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'addresable_id', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['addresable_type', 'title', 'city', 'state'], 'string', 'max' => 127],
            [['line1', 'line2'], 'string', 'max' => 255],
            [['country'], 'string', 'max' => 2],
            [['postal_code'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'addresable_type' => 'Addresable Type',
            'addresable_id' => 'Addresable ID',
            'title' => 'Title',
            'line1' => 'Line1',
            'line2' => 'Line2',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'postal_code' => 'Postal Code',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * @param string $activeRecordNamespace
     * @return \yii\db\ActiveQuery
     */
    public function getAddressable($activeRecordNamespace = 'app\models')
    {
        $activeRecord=$activeRecordNamespace.'\\'.Inflector::camelize($this->addresable_type);

        return $this->hasOne($activeRecord, ['id'=>'addresable_id']);
    }
}
